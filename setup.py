from distutils.core import setup

setup(name='epicsdb',
      version='1.0.0',
      description='Simple linter for EPICS database files',
      author='Karl Vestin',
      author_email='karl.vestin@ess.eu',
      url='https://gitlab.esss.lu.se/karlvestin/epics-db-hook',
      scripts=['epics-db-check.py'],
     )
