#! python
import sys

# Support function to sort warnings by line number
def getLineNo(warning):
    return warning[0]

# Database rules
# Format is [Record_type, Rule, Argument]
# where
# Record_type = The record type for which this rule is applicable. For a rule that is applicable to all record type use "ALL"
# Rule = The type of rule (current supported rule is "SHALL") 
# Argument = Arguments provided for the specific rule
database_rules = [ \
["ALL", "SHALL", "DESC"], \
["ai", "SHALL", "EGU"]]

# Initialize empty lists (to use append later)
warnings = []
database = []

# List of files to check is provided as arguments by pre-commit, but first argument is the name of this script (because reasons I guess)
files = sys.argv
script = files.pop(0)

# Loop through the file list (provided as args from pre-commit)
for file_name in files:
    f = open(file_name, "r")
    lines = f.readlines()
    lineno = 0

    # Loop through each line in the file and create a shallow representation in the database array
    for line in lines:
        lineno = lineno + 1
        whitespaces = len(line)
        line = line.lstrip()
        whitespaces = whitespaces - len(line)
        record_type = ""
        record_line = 0

        # This is the start of a record declaration, document this in the database array and check for whitespaces
        if line.startswith("record"):
            record_type = line[line.find("(") + 1:line.find(",")]
            database.append([lineno, record_type])
            record_line = lineno
            if whitespaces != 0:
                warnings.append([lineno, "[WARNING] Whitespaces before record declaration are not recommended", file_name])

        # This is a field declaration, documentthis in the database array and check whitespaces
        if line.startswith("field"):
            field_type = line[line.find("(") + 1:line.find(",")]
            database[-1].append(field_type)
            if whitespaces != 4:
                warnings.append([lineno, "[WARNING] Four whitespaces are recommended before field declaration", file_name])  
    
    # Done with this file, close file handle
    f.close()

    # Check the database structure
    for record in database:
        fields = record[2:]
        for rule in database_rules:
            # Check if this rule is applicable to this record
            if rule[0] == "ALL" or rule[0] == record[1]:
                # Check SHALL rule (currently redundant check since this is the only rule supported, by added for extensibility) 
                if rule[1] == "SHALL":
                    if not rule[2] in fields:
                        warnings.append([record[0], "[WARNING] " + rule[0] + " records shall declare the " + rule[2] + " field", file_name])

# Print all recorded warnings
warnings.sort(key=getLineNo)
for warning in warnings:
    print(warning[1] + " in " + warning[2] + " (line " + str(warning[0]) + ")") 

# Exit with "Passed" only if there are zero warnings 
sys.exit(len(warnings))
