# epics-db-hook

A simple but extensible linter for EPICS database and template files. The script is intended to be used with a pre-commit hook but can also be used manually

## Requirements
To use this script a python parser must be installed:

```sh
$ sudo yum install python
```

To use this script with pre-commit you must also install pre-commit:

```sh
$ pip install pre-commit
```

Finally to use this as a pre-commit hook a recent version of git is required (which is not available in centos 7 repos):

```sh
$ sudo yum install epel-release
$ sudo yum remove git
$ sudo yum -y install https://packages.endpointdev.com/rhel/7/os/x86_64/endpoint-repo.x86_64.rpm
$ sudo yum install git
```


## Usage with pre-commit

To use this repo as a pre-commit hook add the following to your .pre-commit-config.yaml file

```sh
repos:
  - repo: https://gitlab.esss.lu.se/karlvestin/epics-db-hook.git
    rev: 1.0.0
    hooks:
    - id: epics-db-hook
```

and then run it using:

```sh
$ pre-commit run -a
```

## Usage without pre-commit

To run the script to check files without pre-commit use the following:

```sh
$ python epics-db-check.py epics-db-check.py file1 file2 file3 ....
```

 
## Rules checked

The current version only check the following rules:
1) No whitespaces before the record declaration
2) Exactly 4 whitespaces before a field declaration
3) All records shall defines the "DESC" field
4) Record of the "ai" record type shall declare the "EGU" field

## Contributing

Contributions and suggestions are welcome. Please contact karl.vestin@ess.eu
